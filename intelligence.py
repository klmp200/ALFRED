#! /usr/bin/env python
# -*- coding: utf-8 -*-
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
import socket
import sys
import os

train = len(sys.argv) == 1
chatbot = ChatBot('Alfred',
        input_adapter="chatterbot.input.VariableInputTypeAdapter",
        logic_adapters=[
            'chatterbot.logic.BestMatch',
            'chatterbot.logic.MathematicalEvaluation',
            'chatterbot.logic.TimeLogicAdapter'
        ]
)

if not train:
    server_address = sys.argv[1]

    try:
        os.unlink(server_address)
    except OSError:
        if os.path.exists(server_address):
            raise

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(server_address)
    sock.listen(1)

    while True:
        connection, client_address = sock.accept()
        while True:
            data = connection.recv(1024)
            response = chatbot.get_response(data.decode('utf-8'))
            connection.send(str(response).encode('utf-8'))

else:
    chatbot.set_trainer(ChatterBotCorpusTrainer)
    chatbot.train(
        "chatterbot.corpus.french"
    )
