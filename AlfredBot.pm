#! /usr/bin/env perl

package AlfredBot;

use threads;
use Cwd;
use IO::Socket::UNIX;
use Mojo::Base 'Telegram::Bot::Brain';
use Net::Twitter::Lite::WithAPIv1_1;
use Try::Tiny;
use AlfredBotSettings;

my $cwd = getcwd;
my $socket_addr = "$cwd/alfred_bot";


my $intelligence_thread = threads->new(sub {system("python3 intelligence.py $socket_addr")});
sleep(1); # Wait for the subprogram to launch

my %messages = ();

has token => $AlfredBotSettings::token;
has last_message => %messages;
has message_limit_size => 255;
has twitter => '';
has socket => '';

my $base_url = "https://api.telegram.org/bot$AlfredBotSettings::token";

# Swag functions

sub appologies {
	my ($self, $msg) = @_;
	&quick_message($self, $msg, "Désolé monsieur, il m'est actuellement impossible de récupérer ces informations")
}

sub is_an_integer {
	my $string = shift;
	return $string =~ /^[0-9]+$/
}

sub is_chat_banned {
	my ($self, $msg) = @_;
	return 0 unless $AlfredBotSettings::whitelist{enable};
	my @user_whitelist = @{$AlfredBotSettings::whitelist{users}};
	my @chat_whitelist = @{$AlfredBotSettings::whitelist{chat}};
	return 1 unless defined($msg);
	return 1 unless defined($msg->from);
	return 1 unless defined($msg->chat);
	return 1 unless defined($msg->chat->id);
	for (@chat_whitelist){
		return 0 if $msg->chat->id == $_
	}
	return 1 unless defined($msg->from->username);
	for (@user_whitelist) {
		return 0 if $_ eq $msg->from->username
	}
	return 1
}

sub kick_user {
	my ($self, $msg) = @_;

	if ($msg->chat->is_user){
		&quick_message($self, $msg, "Estimez vous chanceux que nous soyons en conversation privée !")
	} else {
		my $url = "${base_url}/kickChatMember";
		my $chat_id = $msg->chat->id;
		my $user_id = $msg->from->id;

		$self->ua->post($url, form => {chat_id => $chat_id, user_id => $user_id, until_date => 35})
	}
}

sub quick_message {
	my ($self, $msg, $message) = @_;
	$self->send_message_to_chat_id($msg->chat->id, $message) unless ($message eq '')
}

# is this a message we'd like to respond to?
sub _no_spam {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return length $msg->text > $self->message_limit_size
}

sub _game {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	for (qw/jeu joué perte perdre perdu joue joues loose/){
		return 1 if $msg->text =~ /${_}/i
	}
	return 0
}

sub _rickroll {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	if ($msg->text =~ /(rick|troll)/i){
		return 1
	}
	return 0
}

sub _call_alfred {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ /\balfred\b/i
}

sub _get_repo {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ m{^/git$}
}

sub _faggot_name {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	for (qw/aifred a1fred afred/) {
		return 1 if $msg->text =~ /\b${_}\b/i
	}
	return 0
}

sub _spongify {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ m{^/sponge$}
}

sub _trump {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ /\b[cC]ovfefe\b/
}

sub _dice {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ m{/roll}
}

sub _hl3 {
	my ($self, $msg) = @_;
	return 0 if &is_chat_banned($self, $msg);
	return $msg->text =~ /\b(half(-|\s)life)|(hl?3)\b/i
}

# send a polite reply, to either a group or a single user,
# depending on where we were addressed from
sub _be_polite {
	my ($self, $msg) = @_;

	# is this a 1-on-1 ?
	if ($msg->chat->is_user) {
		$self->send_message_to_chat_id($msg->chat->id, "hello there");
	}
	# group chat
	else {
		$self->send_message_to_chat_id($msg->chat->id, "hello to everyone!");
	}
}

sub _game_response {
	&quick_message(@_, "Vous avez perdu au jeu !")
}

sub _rickroll_response {
	&quick_message(@_, "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
}

sub _call_alfred_response {
	my ($self, $msg) = @_;
	#my $username = "Parfait inconnu";
	my $message = $msg->{text} =~ s/\balfred\b//ri;
	my $resp;
	#$username = $msg->from->username if defined($msg->from->username);
	$self->socket->send($message);
	$self->socket->recv($resp, 1024);
	&quick_message($self, $msg, $resp);
	#my @responses = (
	#"Alfred, à votre service.",
	#"Je peux faire quelque chose pour vous monsieur $username ?",
	#"Vous souhaitez quelque chose ?",
	#"Mes fonctionnalitées sont multiples, j'espère pouvoir vous être utile."
	#);
	#if ($username eq 'NaejDoree') { &quick_message($self, $msg, "Veillez vous taire monsieur, vous n'êtes pas le bienvenu !") }
	#else { &quick_message($self, $msg, $responses[rand @responses]) }
}

sub _get_repo_response {
	&quick_message(@_, "Ravi que vous me posiez la question ! Mes sources sont disponnibles ici : https://gitlab.com/klmp200/ALFRED")
}

sub _faggot_name_response {
	&quick_message(@_, "Sorry Steeve...");
	&kick_user(@_);
}

sub _spongify_response {
	my ($self, $msg) = @_;
	my $nb = 0;
	my @message = ();
	if (defined $self->{last_message}{$msg->chat->id}){
		@message = split //, $self->{last_message}{$msg->chat->id}
	}
	my $message = "";
	for (@message){
		my $letter = $_;
		if ($nb){
			$letter = uc($letter);
			$nb = 0
		} else {
			$letter = lc($letter);
			$nb = 1
		}
		$message .= $letter
	}
	&quick_message($self, $msg, $message)
}

sub _no_spam_response {
	my ($self, $msg) = @_;
	my $limit = $self->message_limit_size;
	&quick_message($self, $msg, "Monsieur, la taille limite de message est de $limit. Je vais devoir punir !");
	&kick_user($self, $msg);
}

sub _trump_response {
	my ($self, $msg) = @_;
	my %trump_data = (
		user_id => '25073877',
		screen_name => 'realDonaldTrump'
	);
	try {
		my $trump = $self->twitter->show_user($trump_data{user_id}, $trump_data{screen_name});
		&quick_message($self, $msg, "Voici le derniers tweets de Monsieur Trump :");
		&quick_message($self, $msg, "$trump->{status}{text}\n$trump->{status}{created_at}")
	} catch {
		&appologies($self, $msg)
	};
}

sub _dice_response {
	my ($self, $msg) = @_;
	my ($command, $nb) = split / /, $msg->text;
	if (defined $nb && &is_an_integer($nb)){
		&quick_message($self, $msg, int(rand $nb + 1))
	} else {
		&quick_message($self, $msg, "Les valeurs que vous entrez sont incohérentes monsieur");
	}
}

sub _hl3_response {
	my ($self, $msg) = @_;
	my %hl_data = (
		user_id => '625959894',
		screen_name => 'IsHL3OutYet'
	);
	try {
		my $data = $self->twitter->show_user($hl_data{user_id}, $hl_data{screen_name});
		&quick_message($self, $msg, 
		"$data->{name}\n$data->{description}\n$data->{status}{text}\n$data->{status}->{created_at}"
		)

	} catch {
		&appologies($self, $msg)
	}
}

# setup our bot
sub init {
	my $self = shift;
#	$self->add_listener(
#		\&_hello_for_me,  # criteria
#		\&_be_polite     # response
#	 );

	my $socket = IO::Socket::UNIX->new(
		Type => SOCK_STREAM(),
		Peer => $socket_addr,
	) || die "Error connecting to alfred's intelligence";

	binmode $socket => ":encoding(utf8)";

	$self->{socket} = $socket;

	my %twitter_data = %AlfredBotSettings::twitter;
	$twitter_data{user_agent} = 'Alfred';
	$twitter_data{ssl} = 1;
	try {
		my $twitter = Net::Twitter::Lite::WithAPIv1_1->new(%twitter_data);
		$self->{twitter} = $twitter;
	} catch {
		die "Error connecting to twitter, check your credential and your internet connection"
	};

	$self->add_listener(\&_no_spam, \&_no_spam_response);
	$self->add_listener(\&_game, \&_game_response);
	$self->add_listener(\&_rickroll, \&_rickroll_response);
	$self->add_listener(\&_call_alfred, \&_call_alfred_response);
	$self->add_listener(\&_get_repo, \&_get_repo_response);
	$self->add_listener(\&_faggot_name, \&_faggot_name_response);
	$self->add_listener(\&_spongify, \&_spongify_response);
	$self->add_listener(\&_trump, \&_trump_response);
	$self->add_listener(\&_dice, \&_dice_response);
	$self->add_listener(\&_hl3, \&_hl3_response);

	$self->add_listener(sub {
		my ($self, $msg) = @_;
		return 0 if &is_chat_banned($self, $msg);
		my $limit = $self->message_limit_size;
		$self->{last_message}{$msg->chat->id} = $msg->text unless (length $msg->text > $limit);
		return 0
	}, sub {})


}

END {
	$intelligence_thread->kill('KILL')
}

1;

